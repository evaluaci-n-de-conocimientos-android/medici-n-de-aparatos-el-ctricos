package com.example.mediciondeparametroselectricos;

import android.content.Context;
import android.content.SharedPreferences;

/**-------------------------By Raul Pech----------------------------
 * Esta clase es utilizada en el MainActivity para obtener el estado
 * de inicio de sesion que permitira guardar, si el estado de sesion
 * esta activo o inactivo para que el usuario pueda acceder a su
 * cuennta sin iniciar sesion repetidamente, si este asi lo desea.
 *-----------------------------------------------------------------*/

public class Preferences {
    public static final String KEY_PREFERENCES="com.example.mediciondeparametroselectricos",
                                SAVE_SESSION="mySession.cloud.mediciondeparametroselectricos";

    //Funcion que cambia el estado en el que se encuentra la sesion y lo guarda en
    //SAVE_SESSION.(TRUE/FALSE)
    public static void changeSaveSession(Context context, boolean b,String key){
        SharedPreferences preferences=context.getSharedPreferences(KEY_PREFERENCES,context.MODE_PRIVATE);
        preferences.edit().putBoolean(key,b).apply();
    }

    //Funcion para guardar una respuesta TRUE o FALSE en SAVE_SESSION
    public static void saveSession(Context context, Boolean b,String key){
        SharedPreferences preferences=context.getSharedPreferences(KEY_PREFERENCES,context.MODE_PRIVATE);
        preferences.edit().putBoolean(key,b).apply();
    }

    //Funcion que obtiene el estado inicial que se guarda en SAVE_SESSION.(Estado por defecto "FALSE")
    public static boolean getSaveSession(Context context,String key){
        SharedPreferences preferences=context.getSharedPreferences(KEY_PREFERENCES,context.MODE_PRIVATE);
        return preferences.getBoolean(key,false);
    }
}


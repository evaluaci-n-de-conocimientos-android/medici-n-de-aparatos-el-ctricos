package com.example.mediciondeparametroselectricos;

/**----------------------- By Raul Pech ----------------------------
 * Objeto java que utiliza en la funcion login() de la clase Main.
 * Se utiliiza para hacer parsing de JSON a JAVA Object
 * ----------------------------------------------------------------*/
public class Post {
    private int id;
    private String name,
                username,
                email,
                password;
    public Post(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }
}

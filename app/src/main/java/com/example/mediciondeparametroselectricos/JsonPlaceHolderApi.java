package com.example.mediciondeparametroselectricos;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**-----------------------By Raul Pech---------------------
 * Interfaz que contiene los metodos HTTP para conectar
 * con el servidor.
 * --------------------------------------------------------*/

public interface JsonPlaceHolderApi {

    @POST("aquaculturemobile/ozelot/it/api/login")
    Call<Post> postLogin(@Body Post post);

}

package com.example.mediciondeparametroselectricos;

import java.util.ArrayList;

/**----------------------- By Raul Pech ----------------------------
* Esta clase esta diseñada para dividir la carga de la clase Measurement,
* se pueden encontrar los objetos que serviran para personalizar
* las opciones del RecyclerView ue se utiliza como menu de opciones,
* de igual forma se encuentran las listas y los adaptadores que
* se utilizaran en los RecclerViews
* ------------------------------------------------------------------*/

public class MenuList {
    private ArrayList<MenuObj> menuList;
    private ArrayList <ItemCard> itemList;
    private MenuAdapter menuAdapter;
    private ItemAdapter  itemAdapter;
    private MenuObj edificios;
    private MenuObj sensores;

    //Constructor
    public MenuList() {
        this.menuList = new ArrayList<>();
        this.itemList = new ArrayList<>();
        this.menuAdapter = new MenuAdapter(menuList);
        this.itemAdapter = new ItemAdapter(itemList);
        this.edificios = new MenuObj("Edificios",R.drawable.edificios,R.drawable.arrowrigth);
        this.sensores = new MenuObj("Sensores principales",R.drawable.sensores,R.drawable.arrowup);
    }

    //Metodo getter del ItemAdapter
    public ItemAdapter getItemAdapter() {
        return itemAdapter;
    }

    //Metodo getter del MenuAdapter
    public MenuAdapter getMenuAdapter() {
        return menuAdapter;
    }

    //Metodo getter del Objeto creado apartir de la clase Menobj.
    public MenuObj getEdificios() {
        return edificios;
    }

    //Metodo getter del Objeto creado apartir de la clase Menobj.
    public MenuObj getSensores() {
        return sensores;
    }

    //Funcion de llenado de la lista menuList.
    public void  fillMenu(){
        menuList.add(edificios);
        menuList.add(sensores);
    }

    //Funcion de llenado de la lista itemList.
    public void fillItem(){
        //La funcion .add() añade el contenedor que se mostrara en la lista del RecyclerView
        //con los datos que se contendran en este.
        itemList.add(new ItemCard("Sensor 1","1887.6","120","15.73"));
        itemList.add(new ItemCard("Sensor 2","1556.23","120.5","18.360"));
        itemList.add(new ItemCard("Sensor 3","2300","120","16.83"));
    }
}

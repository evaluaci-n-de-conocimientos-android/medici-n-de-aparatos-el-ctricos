package com.example.mediciondeparametroselectricos;

/**----------------------- By Raul Pech ----------------------------
 * Clase creada para instanciar objetos que se pasen por el adaptador
 * personalizado de la clase ItemAdapter
 *------------------------------------------------------------------*/
public class ItemCard {
    private String sensor=null,
                    kw=null,
                    amper=null,
                    volt=null;
    public ItemCard(String sensor, String kw, String volt, String amper){
        this.sensor=sensor;
        this.kw=kw;
        this.volt=volt;
        this.amper=amper;
    }
    public String getSensor() {
        return sensor;
    }

    public void setSensor(String sensor) {
        this.sensor = sensor;
    }

    public String getKw() {
        return kw;
    }

    public void setKw(String kw) {
        this.kw = kw;
    }

    public String getAmper() {
        return amper;
    }

    public void setAmper(String amper) {
        this.amper = amper;
    }

    public String getVolt() {
        return volt;
    }

    public void setVolt(String volt) {
        this.volt = volt;
    }
}

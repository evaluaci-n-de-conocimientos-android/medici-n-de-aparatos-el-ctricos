package com.example.mediciondeparametroselectricos;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

/**----------------------- By Raul Pech ----------------------------
 * Clase encargada de hacer un adaptador personalizado para el
 * RecyclerView que solo mostrara datos debido a que el RecyclerView.
 * (Esta clase utiliza el layout card_menu.xml)
 *----------------------------------------------------------------*/

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemHolder> {
    private ArrayList<ItemCard> item;

    // clase que hereda de RecyclerView.ViewHolder
    public class ItemHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout itemContainer;
        private TextView sensor,
                kw,
                volt,
                amper;
        //Enlace de los recursos del layout
        public ItemHolder(View itemView) {
            super(itemView);
            itemContainer=(ConstraintLayout) itemView.findViewById(R.id.itemContainer);
           sensor=(TextView)itemView.findViewById(R.id.txtSensor);
           kw=(TextView)itemView.findViewById(R.id.txtKW);
           volt=(TextView)itemView.findViewById(R.id.txtVoltaje);
           amper=(TextView)itemView.findViewById(R.id.txtAmper);
        }
    }

    //Constructor
    public  ItemAdapter(ArrayList<ItemCard> item){
        this.item=item;
    }

    //Metodo getter que retorna los elementos en una lista
    public ArrayList<ItemCard> getItems() {
        return this.item;
    }

    //Esta clase se encarga de relacinar los elementos utilizados en el layout
    //card_menu.xml dentro de un contenedor
    @Override
    public ItemHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view= LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.card_menu,viewGroup,false);
        return  new ItemHolder(view);
    }

    //Metodo que relaciona los recursos que se le pasan conforme a la posicion de la lista
    @Override
    public void onBindViewHolder(ItemHolder itemHolder, int i) {
            itemHolder.sensor.setText(item.get(i).getSensor());
            itemHolder.kw.setText(item.get(i).getKw());
            itemHolder.volt.setText(item.get(i).getVolt());
            itemHolder.amper.setText(item.get(i).getAmper());
    }

    //Metodo getter que retorna el tamaño de los objetos de la lista ItemCard
    @Override
    public int getItemCount() {
        return item.size();
    }

}

package com.example.mediciondeparametroselectricos;

/**----------------------- By Raul Pech ----------------------------
* Clase creada para instanciar objetos que se pasen por el adaptador
* personalizado de la clase MenuAdapter
*------------------------------------------------------------------*/
class MenuObj {
    private String option=null;
    private int imgOption=0,
                    arrow=0;

    public MenuObj(String option, int imgOption, int arrow){
        this.option=option;
        this.imgOption=imgOption;
        this.arrow=arrow;
    }
    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public int getImgOption() {
        return imgOption;
    }

    public void setImgOption(int imgOption) {
        this.imgOption = imgOption;
    }

    public int getArrow() {
        return arrow;
    }

    public void setArrow(int arrow) {
        this.arrow = arrow;
    }
}

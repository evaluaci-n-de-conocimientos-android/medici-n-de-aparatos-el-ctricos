package com.example.mediciondeparametroselectricos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextUser,
                    editTextPassword;
    private TextView textView;
    private Toolbar toolbar;
    private Button btnLogin;
    private CheckBox checkBoxSession;
    private Intent i;
    private JsonPlaceHolderApi jsonPlaceHolderApi;

    //Metodo Oncreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView=findViewById(R.id.request);

        //TODO>Incrustacion del toolbar
        buildToolbar();

        //TODO>Incrustacion de los botones
        buildButtom();

        //TODO>Incrustacion del Request
        buildRetrofitRequest();

        //TODO>Incrustacion del estado de la sesion obtenido por el CheckBox(Estado por defecto FALSE)
        if(Preferences.getSaveSession(MainActivity.this,Preferences.SAVE_SESSION)){
                i = new Intent(getApplicationContext(), Measurement.class);
                startActivity(i);
                finish();
        }
    }

    //Funcion que constrye los eventos que se relacionan con el Toolbar
    private void buildToolbar(){
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    //Funcion que constrye los eventos que se relacionan con el boton
    private void buildButtom(){
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        checkBoxSession=findViewById(R.id.checkBoxSession);
        checkBoxSession.setOnClickListener(this);
    }

    //Funcion que constrye la direccion url para hacer peticiones al servidor
    private void buildRetrofitRequest(){
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("http://coffeefood.com.mx/apirest.coffeefood.com.mx/public/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi=retrofit.create(JsonPlaceHolderApi.class);
    }

    //Funcion que hace utilidad de la clase POST para hacer el parsing del JSON, en este caso se
    //utiliza el endpoint que valida a los usuarios y se llama desde este metodo
    private void login(){
        editTextUser=findViewById(R.id.editTextUser);
        editTextPassword=findViewById(R.id.editTextPassword);

        String user=editTextUser.getText().toString(),
                password=editTextPassword.getText().toString();
        Post post=new Post(user,password);
        Call<Post> call =jsonPlaceHolderApi.postLogin(post);
                //Validacion del EditText del Usario en caso de estar vacio
            if (user.isEmpty()){
                editTextUser.setError("El usuario es obligatorio");
            }
                //Validacion del EditText del Passwword en caso de estar vacio
            if (password.isEmpty()){
                editTextPassword.setError("La contraseña es obligatoria");
            }
                //Validacion de los EditText Usuario y Passwword en caso de estar vacios
            if (user.isEmpty() && password.isEmpty()) {
                editTextUser.setError("El usuario es obligatorio");
                editTextPassword.setError("La contraseña es obligatoria");
                }
            else {
            call.enqueue(new Callback<Post>() {
                @Override
                public void onResponse(Call<Post> call, Response<Post> response) {
                    Post postResponse = response.body();
                        //Validaciion en caso de establecer conexion con el servidor
                    if (!response.isSuccessful()) {
                        String content = "";
                        //content += "error: codigo de error " + response.code();
                        content += "error '" + response.message() + "'\nUsuario o contraseña invalido";
                        textView.setText(content);
                        textView.setTextColor(Color.RED);
                        return;
                    } else {
                        String content ="Bienvenido";
                        content += " usuario \n'" + postResponse.getUsername() + "'";
                        textView.setText(content);
                        textView.setTextColor(Color.GREEN);

                        //Obtiene el estado del CheckBox y lo guarda en SAVE_SESSION
                        Preferences.saveSession(MainActivity.this,checkBoxSession.isChecked(),Preferences.SAVE_SESSION);
                        //Toast.makeText(getApplicationContext(), "estado:"+Preferences.getSaveSession(MainActivity.this,Preferences.SAVE_SESSION), Toast.LENGTH_SHORT).show();

                        //Cambio de actiivvidad
                        i = new Intent(getApplicationContext(), Measurement.class);
                        startActivity(i);
                        finish();
                    }
                }
                //Metodo que se activa al fallar la petcion al servidor
                @Override
                public void onFailure(Call<Post> call, Throwable t) {
                    //textView.setText(t.getMessage());
                    textView.setText("conectate a internet");
                }
            });
        }
    }

    //Metodo que permite crear un evento 'click'
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                login();
                break;
        }
    }
}


package com.example.mediciondeparametroselectricos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.Menu;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import java.util.ArrayList;

/**----------------------- By Raul Pech ----------------------------
* Clase que se encargada de enlazar los elementos en el layout
* measurement.xml (clase de la pantalla de medicion).
* ----------------------------------------------------------------*/
public class Measurement extends AppCompatActivity {
    private Intent i;
    private Toolbar toolbar;
    private RecyclerView recycleMenu,
                        recyclerItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement);

        //TODO>Incrustacion del Toolbar
        buildToolbar();

        //TODO>Incrustacion del RecyclerView
        buildRecyclerView();
    }
        //Funcion que constrye los eventos que se relacionan con el Toolbar
        private void buildToolbar(){
            toolbar=findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
        }
        public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.item_menu,menu);
        return true;
    }

        //Metodo que crea los eventos 'click' de los items del TollBar
        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()){
                case R.id.logout:
                    //"Preferences.changeSaveSession()" proviene de la clase Preferences, se utiliza
                    //en este caso en el apartado cerrar sesion, al hacer click en esta se cerrara la
                    //sesion activa
                    Preferences.changeSaveSession(Measurement.this,false,Preferences.SAVE_SESSION);

                    i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    finish();
                break;
                case R.id.notify:
                    Toast.makeText(this,"No hay notificaciones",Toast.LENGTH_SHORT).show();
                break;
            }
            return super.onOptionsItemSelected(item);
        }

        //Funcion que constrye los eventos que se relacionan con el RecclerView
        private void buildRecyclerView(){
            //Objeto de la clase MenuList que contiene los adaptadores personalizados de los RecyclerViews,
            final MenuList list=new MenuList();

            //funciones de llenado de los RecyclerViews
            list.fillMenu();
            list.fillItem();

            //Obtener Recyler
            recycleMenu=findViewById(R.id.recyclerMenu);
            recyclerItems=findViewById(R.id.recyclerViewItem);

            //Asignacion del layout
            recycleMenu.setLayoutManager(new LinearLayoutManager(this));
            recyclerItems.setLayoutManager(new LinearLayoutManager(this));

            //Asignacion del adaptador
            recyclerItems.setAdapter(list.getItemAdapter());
            recycleMenu.setAdapter(list.getMenuAdapter());

            //Metodo que añade el evento 'click'
            list.getMenuAdapter().setOnItemListListener(new MenuAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    switch (position){
                        case 0:
                            Toast.makeText(Measurement.this, "Opcion desactivada", Toast.LENGTH_SHORT).show();
                            list.getMenuAdapter().notifyItemChanged(position);
                            break;
                        case 1:
                            list.getMenuAdapter().notifyItemChanged(position);
                            //Validacion que veriifica si el estado del RecyclerView es visible
                            if (recyclerItems.getVisibility()!=View.VISIBLE){
                                recyclerItems.setVisibility(View.VISIBLE);
                                list.getSensores().setArrow(R.drawable.arrowup);
                            }else {
                                recyclerItems.setVisibility(View.INVISIBLE);
                                list.getSensores().setArrow(R.drawable.arrowrigth);
                            }
                            recycleMenu.setAdapter(list.getMenuAdapter());
                            break;
                        default:
                            break;
                    }
                }
            });
        }
}

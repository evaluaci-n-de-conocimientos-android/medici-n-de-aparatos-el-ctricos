package com.example.mediciondeparametroselectricos;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

/**----------------------- By Raul Pech ----------------------------
 * Clase encargada de hacer un adaptador personalizado para el
 * RecyclerView con una interfaz que contiene un metodo OnItemClick,
 * debido a que el RecyclerView no incluye un metodo que agregue
 * eventos de click. (Esta clase utiliza el layout item_list.xml)
 *----------------------------------------------------------------*/

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.Dataholder> {
    private ArrayList<MenuObj> menuObjs;
    private OnItemClickListener listener;

    //Constructor
    public  MenuAdapter(ArrayList<MenuObj> menuObjs){
        this.menuObjs = menuObjs;
    }

    //Metodo getter que retorna los elementos en una lista
    public ArrayList<MenuObj> getItems() {
        return this.menuObjs;
    }

    //Creacion de la interfaz para crear un metodo onItemClick
    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    //Metodo para agregar eventos de clic a las listas del RecyclerView
    public void setOnItemListListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    //Esta clase se encarga de relacinar los elementos utilizados en el layout
    // item_list.xml dentro de un contenedor
    public class Dataholder extends RecyclerView.ViewHolder {
        //ConstraintLayout containerMenu;
        CardView containerMenu;
        TextView txtMenu;
        ImageView logo,
                arrow;

        public Dataholder(View v, final OnItemClickListener listener) {
            super(v);
            containerMenu = (CardView) v.findViewById(R.id.containerMenu);
            txtMenu = (TextView) v.findViewById(R.id.txtMenu);
            logo = (ImageView) v.findViewById(R.id.logo);
            arrow = (ImageView) v.findViewById(R.id.arrow);

            //funcion encaarda de agregar un evvento clic al precionar el elemento de la lista
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();

                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    //Metodo getter que retorna el tamaño de los objetos de la lista menuObj
    @Override
    public int getItemCount() {
        return menuObjs.size();
    }

    //Metodo que retorna el layout de los elemetos dentro de un objeto Dataholder
    @Override
        public Dataholder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v= LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.item_list,parent,false);
            return  new Dataholder(v,listener);
        }

    //Metodo que relaciona los recursos que se le pasan conforme a la posicion de la lista
    @Override
    public void onBindViewHolder(Dataholder holder, int position) {
        holder.logo.setImageResource(menuObjs.get(position).getImgOption());
        holder.txtMenu.setText(menuObjs.get(position).getOption());
        holder.arrow.setImageResource(menuObjs.get(position).getArrow());
    }
}
